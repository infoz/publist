# publist - an auto-updated publication list for departments or research groups

The publist application consists of three parts: 
- a python script that fetches publications from Scopus and stores them in a database,
- a php script that acts as an API to make the data available to a website, 
- the website that displays the list to users

## Prerequisites
- an institutional Scopus subscription - this is required to be allowed to store the publication data of the institution
- a server and mysql database
- python and the following python libraries:
    - pybliometrics (https://pybliometrics.readthedocs.io/en/stable/, tested version 3.0.1)
    - mysql-connector-python

## Prepare the database
1. Add mysql users: 
    - *user_r* for the php-API: needs only read permissions (GRANT SELECT)
    - *user_rw* to be used by the python script that will update the database, needs at least SELECT, UPDATE, INSERT, DELETE permissions; for creating (or recreating) the tables we also need CREATE, DROP, REFERENCES permissions, we grant those also to *user_rw*; one might decide to use a different, admin user for that

2. Copy the content of python-db to a folder on a server with access to your database.
3. Adapt the dbconfig.json file with the credentials of *user_rw*

4. Create the tables: this can be done by pasting the content of create-tables.sql to the mysql-commandline or by running:
    ```
    ./create_tables.py -sql create_tables.sql -c dbconfig.json
    ```
6. Add PIs to the "pi" table. If a csv-file containing all required fields has been prepared (see example file pis_example.csv) they can be added from the file during database creation:
    ```
    ./create_tables.py -sql create_tables.sql -c dbconfig.json -pis pis.csv
    ```
    or afterwards:
    ```
    ./csv2db.py -t pi -csv pis.csv -c dbconfig.json
    ```

## Fetch publications
1. Add your information to config.json:
    - *emailsender*: email address that should be used by the server to send logs or new paper alerts
    - *emailrecipient*: the (admin) user that should receive a log file when the script is run with --sendmail, can be left empty without --sendmail
    - *recipients_papers*: a list of addresses, who should get notified when new papers arrive when the script is run with --newpaperalert, can be an empty list "[]"
    - *logfilepath*: location for the log file, will be overwritten every time the script runs, leave empty if you do not want to write a log file
    - *website*: the URL of the publication list website, this is only used to provide a direct link in the "new paper alert"-emails and can be skipped or left empty
2. Make sure *config.json* and *dbconfig.json* are available at the paths specified at the top of publications-to-database.py,  modify them if necessary.
3. Run publications-to-database.py, e.g.
    ```
    ./publications-to-database.py -y 2020 -m -npa
    ```
   When you run the script for the first time, pybliometrics will ask for your Scopus API key. It will then by default store it in ~/.scopus/config.ini (more info: https://pybliometrics.readthedocs.io/en/stable/configuration.html).

   You will probably want to create a cronjob that will run it daily from now on to keep the database up-to-date.

## Set up the php-API
1. Put server/config.php in a folder on a server that is not below the document root and edit the database credentials in it (this should be *user_r* the mysql user with only SELECT rights). 
2. Put server/publist.php in a folder in the document root of the same server and at the top of the file edit the path to the configuration file config.php

## Display the publication list
1. Edit the three variables in the header of website/publist.html:
   - *proxyurl*: the URL of publist.php
   - *researchgroup*: if only publications from one researchgroup, i.e. one PI, should be shown, fill in the name of the PI (as it occurs in the pi table) here, else set it to ""
   - *institute*: if only publications of one institute should be shown, fill in the short name of the institute (as it is in the institute column of the pi table), else set it to ""
  Always only set either the institute OR the researchgroup variable.

2. For testing purposes put the content of website/ in the document root of a server, you should be able to view the publication list now. To incorporate the list in a webpage or somewhere in a web content management system, the section in publist.html marked as "PUBLIST STYLE AND SCRIPTS" should be in the head of the web page and all the .js and .css files available at the specified paths, the section marked as "PUBLIST HTML" should be in the page body.

----
## Further Reading: Database descriptions
### Table "pi"
- *auid*: Scopus author ID
- *name*: name of the PI, this is also the way it will be displayed in the dropdown list of the website
- *alt_auid1*-*5*: up to 5 alternative auids that have also been found for this PI, 0 if not used
- *orcid*: optional, not used currently
- *institute*: optimally an abbreviation of the institute, if the publication list is not filtered by PI or institute, the institute will be shown in brackets after the PI's name
- *emeritus*: emeritus status (0 or 1), this information is not currently used
- *noprof*: actively publishing but not a professor (0 or 1), this information is not currently used
- *exclude*: do not show in the publication list
- *exit_year*: if the PI has left the department, this is the year he left in (default: 0000); publicatons-to-database.py will not collect publications published later than two years after exit_year

###  Table "publications"
- *eid*: Scopus publication identifier
- *doi*
- *authors*
- *journal*
- *volume*
- *issue*
- *pagerange*: if there is no pagerange, but an articlenumber (sometimes for online publications), the article number is displayed
- *coverdate*: coverdate as provided by Scopus for this publication
- *title*
- *pubmed_id*: collected but not used
- *timestamp*: date this publication was last modified
- *status*: 
  - 0: no special status (default)
  - 1: manually corrected, publications-to-database.py will not update the following fields of this entry: "authors", "journal", "volume", "title", "issue", "pagerange" - if there is a mistake in the Scopus data, e.g. special characters displayed wrong in the title or a typo in an author name, one can manually correct these in the database and then set status to 1 so that the manual correction will not be overwritten
  - 2: used to mark manually added entries (when adding entries manually that are not in Scopus, a unique identifier has to be chosen for the eid)
  - 3: exclude, will be ignored by the php-API
- *date_added*: date this publication was first added to the database; as for some publications the coverdate lies very much in the future and they would remain at the top of the list for almost the whole year if sorted by coverdate, we sort the list by the date_added; however, since some publications newly appearing on Scopus are not new, if the coverdate of a new publication is from a past year, the date_added is set to the cover_date
- *orphan*: the publication is in the database, but not in Scopus any more (orphans should be checked once in a while, sometimes it happens that a publication is removed and re-added with a new eid, this will lead to an orphan in our database with the old eid)
- *subtype*: the subtypeDescription field of Scopus, at the moment this information is used to filter out Errata in publist.php

### Connection table "pi_to_pubs"
connects the pi and publications table (some publications are connected to more than one pi)
- *eid*: Scopus publication identifier
- *auid*: Scopus author ID
- *alt_auid*: alternative auid of this author, with which this publication was found, 0 if it was the main one (field auid)
- *multiple_affiliations*: 1 if the author had multiple affiliations on this publication (field is not used)
- *timestamp*: date this db entry was last modified 


### Tables with information that is not used in the current implementation of the publication list
One could remove their occurrences in publications-to-database.py and discard them.
- "affiliations": stores all affiliations a PI has had in any of the publications
- "pi_to_affil": connects auids (author IDs) with the afids (affiliation IDs)
- "pubs_to_affil": connects publications via their Scopus eids to the affiliations (their afids) the PI had on them
- "metrics": stores citation numbers from Scopus and Altmetric scores and badges, the latter information is now embedded in the webpage via javascript code provided by Altmetric, so this table is not needed any more

##  Useful SQL statements
Create users:
```
CREATE USER user_r@localhost IDENTIFIED BY 'passworD1,1';
GRANT SELECT ON testdb.* TO user_r@localhost;
CREATE USER user_rw@localhost IDENTIFIED BY 'passworD2,1';
GRANT CREATE, REFERENCES, DROP, SELECT, UPDATE, INSERT, DELETE ON testdb.* TO user_rw@localhost;
```
If you run into an error "_mysql_connector.MySQLInterfaceError: Authentication plugin 'caching_sha2_password' cannot be loaded", change the password type using this:
```
ALTER USER user_r@localhost IDENTIFIED WITH mysql_native_password BY 'passworD1,1';
ALTER USER user_rw@localhost IDENTIFIED WITH mysql_native_password BY 'passworD2,1';
```

View all PIs:
```
select * from pi order by name;
```

Get all publications by a PI in a certain year:
```
SELECT * FROM pi a, publications b, pi_to_pubs c WHERE a.auid=c.auid AND b.eid=c.eid AND a.name = "Seebach, Dieter" AND YEAR(coverdate)="2020";
```

Get the affiliations a PI has published with:
```
SELECT * FROM pi a, affiliations b, pi_to_affil c WHERE a.auid=c.auid AND b.afid=c.afid AND a.name = "Seebach, Dieter";
```

Manually modify the title of a publication (and set status to "manually corrected"):
```
UPDATE publications SET title="..", status=1 WHERE eid="2-s2.0-85084104229";
```
