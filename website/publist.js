$( function () {

  // initialize
  if (typeof researchgroup !== "undefined" && researchgroup != '') {
    $("#rgroup-block").hide();
    $("#rgname-input").val(researchgroup);
  }
  else if (typeof institute !== "undefined" && institute != '') {
    $("#institute-input").val(institute);
  } else {
    $("#rgname-input").val("");
    $("#institute-input").val("");
  }

  var query_parameters = deserialize($('#myform').serialize());
  pi_array = getpis(query_parameters);

  //

  function getpis(query_parameters) {
    var ser = "getpis=1";
    //console.log("get " + proxyurl + "?" + ser + "&" + serialize(query_parameters));

    pi_array = {};
    $.ajax({
      url: proxyurl,
      data: ser + "&" + serialize(query_parameters),
      dataType: "JSON",
      success: function (pis) {
        $.each(pis, function (i, pi) {
          var optiontext = pi.name;
          if (typeof institute === "undefined" || institute == "") {
            optiontext += " (" + pi.institute + ")";
          }
          $('#rgroup-select').append($('<option>', {
            value: pi.auid,
            text: optiontext
          }));
          pi_array[pi.name] = pi.auid;

        });

        if (query_parameters["rgname"] && query_parameters["rgname"] != "") {
          var auid = pi_array[query_parameters["rgname"]]
          query_parameters["rgroup"] = auid;
          $("#rgroup-select").val(auid);
        }
        /*do an initial search*/
        getpublicationslist(query_parameters);

      },
      error: function (jqXHR, textStatus, errorThrown) {
        $('.ajax').text(jqXHR, textStatus, errorThrown);
        console.log(jqXHR, textStatus, errorThrown);
      }
    });
    return pi_array;
  }

  function serialize(obj) {
    // create query string from object
    var str = [];
    for (var p in obj)
      if (obj.hasOwnProperty(p)) {
        str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
      }
    return str.join("&");
  }

  function deserialize(ser) {
    // create object from query string
    ser = decodeURIComponent(ser);
    ser = ser.replace(/^\?/, '');
    ser = ser.replace(/\+/g, ' ');
    var o = {};
    var a = ser.split("&");
    a.forEach((item) => {
      kv = item.split("=");
      o[kv[0]] = kv[1];
    });
    return o;
  }

  function getpublicationslist(queryobj) {
    $("#paperresults > tbody").empty();

    if (typeof queryobj == "undefined") {
      queryobj = deserialize($('#myform').serialize());
    }

    queryobj["togglealt"] = ($("#togglealt").is(":checked")) ? "1" : "0";

    var serializedresultstring = serialize(queryobj);
    //console.log('get: ' + proxyurl + "?" + serializedresultstring);
    $.ajax({
      url: proxyurl,
      data: serializedresultstring,
      dataType: "JSON",
      success: function (data) {
        if (data['total'] > 0) {
          var publications = data['publications'];
          for (var i = 0; i < publications.length; i++) {
            var altmtrcviaDOI = (publications[i]['doi']) ? (publications[i]['doi']) : ('');
            var papertitle = (publications[i]['title']) ? (publications[i]['title']) : ('');
            var authors = (publications[i]['authors']) ? (publications[i]['authors']) : ('');
            var journal = (publications[i]['journal']) ? ('<i>' + publications[i]['journal'] + ', </i>') : ('');
            var volume = (publications[i]['volume']) ? (publications[i]['volume'] + ', ') : ('');
            var issue = (publications[i]['issue']) ? (publications[i]['issue'] + ', ') : ('');
            var pagerange = (publications[i]['pagerange']) ? (publications[i]['pagerange'] + ', ') : ('');
            var coverdate = (publications[i]['coverdate']) ? (publications[i]['coverdate'] + ' ') : ('');
            var doilink = (publications[i]['doi']) ? ('<a href="https://doi.org/' + publications[i]['doi'] + '" class="eth-link" target="_blank">DOI<span aria-hidden="true" class="material-icons">call_made</span></a>') : ('');

            /*display the papers found*/
            $("#paperresults > tbody").append('<tr>' +
              '<td class="publication"><b>' + papertitle + ' </b><br>' +
              authors + ' <br>' +
              journal + volume + issue + pagerange + coverdate + doilink +
              '</td>' +
              '<td class ="altmetric-badge"><div data-badge-popover="left" data-badge-type="donut" data-doi="' + altmtrcviaDOI + '" data-hide-no-mentions="true" class="altmetric-embed"></div></td>' +
              '</tr>');
          }
        }

        /*display number of hits*/
        if (data['total'] > 1) {
          $(".publicationresultcount").html('<b><small>' + data['total'] + ' publications found</small></b>');
        } else if (data['total'] == 1) {
          $(".publicationresultcount").html('<b><small>' + data['total'] + ' publication found</small></b>');
        } else {
          $(".publicationresultcount").html('<b><small>No publications found</small></b>');
        }

        /*display pagination links*/
        if (typeof data['pagination'] !== "undefined") {
          $(".paginationsidebyside").html(data['pagination']);
        } else {
          $(".paginationsidebyside").html("");
        }

        /*Altmetric score and checkbox on or off?*/
        if (data['togglealt'] == 1) {
          $(".altmetric-badge").show();
          $("#togglealt").prop('checked', true);
        }

        _altmetric_embed_init(document.getElementById('table-matrix'));
        _altmetric_embed_init('#table-matrix');

      },
      error: function (jqXHR, textStatus, errorThrown) {
        $('.ajax').text(jqXHR, textStatus, errorThrown);
        console.log(jqXHR, textStatus, errorThrown);
      }
    });
  }


  $('#gosearch').click(function () {
    getpublicationslist(deserialize($('#myform').serialize()));
  });

  //
  $('.paginationsidebyside').on("click", "a", function (event) {
    event.preventDefault();

    var paginationhref = $(this).attr("href");
    var queryobj = deserialize(paginationhref);

    getpublicationslist(queryobj);
  });


  $(".altmetriccheckboxlabel").click(function () {
    if ($("#togglealt").is(":checked")) {
      $(".altmetric-badge").fadeOut();
      $("#togglealt").prop('checked', false);
      $('.altmetriccheckboxlabel').text('Show Altmetric data');
    }
    else {
      _altmetric_embed_init(document.getElementById('table-matrix'));
      _altmetric_embed_init('#table-matrix');
      $(".altmetric-badge").fadeIn();
      $("#togglealt").prop('checked', true);
      $('.altmetriccheckboxlabel').text('Hide Altmetric data');
    }
  });
  
  $("#infoicon").click(function () {
    if ($("#togglealt").is(":checked")) {
      $(".altmetric-badge").fadeOut();
      $("#togglealt").prop('checked', false);
      $('.altmetriccheckboxlabel').text('Show Altmetric data');
    }
    else {
      _altmetric_embed_init(document.getElementById('table-matrix'));
      _altmetric_embed_init('#table-matrix');
      $(".altmetric-badge").fadeIn();
      $("#togglealt").prop('checked', true);
      $('.altmetriccheckboxlabel').text('Hide Altmetric data');
    }
  });


  $('#myform').keypress(function (e) {
    
    if (e.which == 13) { // execute upon Enter
      getpublicationslist(deserialize($('#myform').serialize()));
      return false;    //<---- Add this line
    }
  });

  // reset the form and delete the results
  $('#reset').click(function () {
    $('#myform')[0].reset();
    $("#paperresults > tbody").empty();
    $(".publicationresultcount").empty();
    $(".paginationsidebyside").empty();
  });

});
