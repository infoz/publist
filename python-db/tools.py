import os, sys
import json
import time
import logging
import smtplib
import requests
from email.mime.text import MIMEText
from pybliometrics.scopus import ScopusSearch
from pybliometrics.scopus.exception import ScopusHtmlError
    
def send_email_with_text(message, subject, recipient, sender):

    themsg = MIMEText(message)
    themsg['Subject'] = subject
    themsg['To'] = recipient
    themsg['From'] = sender
    themsg = themsg.as_string()

    smtp = smtplib.SMTP()
    smtp.connect()
    smtp.sendmail(sender, recipient, themsg)
    smtp.close()


def get_profs_from_DB(dbs, profnames, dbtable="pi"):
  vals = []
  where = None
  if len(profnames) > 0:
      wherestr = "name LIKE " + \
          " OR name LIKE ".join(["%s" for x in profnames])
      for profname in profnames:
        vals += ["%" + profname + "%"]
      where = (wherestr, vals)
  fields = ["auid", "name", "alt_auid1", "alt_auid2", "alt_auid3",
            "alt_auid4", "alt_auid5", "institute", "exit_year", "orcid"]
  professors = dbs.getAll(dbtable, fields, where=where, order=["name", "ASC"])

  if not professors:
      sys.exit("NOTE: %s table is empty"%(dbtable))

  for prof in professors:
      alt_auids = [prof[k] for k in ["alt_auid1", "alt_auid2",
                                      "alt_auid3", "alt_auid4", "alt_auid5"] if prof[k] != "0"]
      prof['auids'] = [prof["auid"]]+alt_auids
      logging.debug("adding %s, %s, auids: %s: " %
                    (prof["name"], prof["institute"], prof["auids"]))
  return professors

def get_author_publications_from_scopus(auids, year=None, refresh=0):
  """ get all publications for these authorids
      optionally restricted to a specific year
  """
  query = " OR ".join("AU-ID(%s)" % (auid) for auid in auids)
  if year:
    query += " AND PUBYEAR IS %s" % (year)
  logging.debug("querying scopus for " + query)
  dic = {}
  num_errors=0
  error=True
  retry_after = 10 # if there is an error, retry after x seconds

  while error and num_errors<5:
    try:
      s = ScopusSearch(query, refresh=refresh)
      error=False
      if s.get_results_size() > 0:
        for pub in s.results:
          dic[pub.eid] = pub
    except ScopusHtmlError as err:
      emsg="ScopusHtmlError: %s" % (err)
      error=True
      num_errors+=1
      time.sleep(retry_after)
    except requests.exceptions.HTTPError as err:
      emsg="HTTPError: %s" % (err)
      error=True
      num_errors+=1
      time.sleep(retry_after)
    except requests.exceptions.RequestException as err:
      emsg="RequestException: %s" % (err)
      error=True
      num_errors+=1
      time.sleep(retry_after)
  if error:
    logging.warning("%s"%(emsg))
    logging.warning("  tried 5 times but could not fetch results for: %s, try again next time\n"%(query))

  return dic


def get_author_affiliations(pub, seq):
  """ get affiliations of the "seq"-th author of
      the publication

      pub .. a pybliometrics publication Object
  """
  aff = {}
  if pub.author_afids:
    try:
      aafids = pub.author_afids.split(";")[seq].split("-")
    except IndexError:
      # this can happen if there are too many authors
      return aff

    if pub.afid:
      afids = pub.afid.split(";") if pub.afid else []
      cities = pub.affiliation_city.split(
          ";") if pub.affiliation_city else [None]*len(afids)
      countries = pub.affiliation_country.split(
          ";") if pub.affiliation_country else [None]*len(afids)
      affilnames = pub.affilname.split(";") if pub.affilname else [
          None]*len(afids)

      i = 0
      for afid in afids:
        if afid in aafids:
          aff[afid] = {"afid": afid, "city": cities[i],
                        "country": countries[i], "affilname": affilnames[i]}
        i += 1
  return aff


def clean_title(pub):
  title = pub.title.replace("<inf>", "<sub>").replace("</inf>", "</sub>")
  return title


def clean_pagerange(pub):
  pr = pub.pageRange
  if not pr and pub.article_number != None:
    pr = pub.article_number
    logging.debug("%s using article number as pagerange" % (pub.eid))

  return pr

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    BOLD = '\033[1m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

    def disable(self):
        self.HEADER = ''
        self.OKBLUE = ''
        self.OKGREEN = ''
        self.WARNING = ''
        self.FAIL = ''
        self.ENDC = ''

# Custom formatter: https://stackoverflow.com/questions/14844970/modifying-logging-message-format-based-on-message-logging-level-in-python3
class MyFormatter(logging.Formatter):

    err_fmt  = "ERROR: %(msg)s"
    warn_fmt  = "WARNING: %(msg)s"
    dbg_fmt  = "DEBUG: %(module)s: %(lineno)d: %(msg)s"
    info_fmt = "%(msg)s"

    def __init__(self):
        super().__init__(fmt="%(levelno)d: %(msg)s", datefmt=None, style='%')  

    def format(self, record):

        # Save the original format configured by the user
        # when the logger formatter was instantiated
        format_orig = self._style._fmt

        # Replace the original format with one customized by logging level
        if record.levelno == logging.DEBUG:
            self._style._fmt = MyFormatter.dbg_fmt

        elif record.levelno == logging.INFO:
            self._style._fmt = MyFormatter.info_fmt

        elif record.levelno == logging.WARNING:
            self._style._fmt = MyFormatter.warn_fmt

        elif record.levelno == logging.ERROR:
            self._style._fmt = MyFormatter.err_fmt

        # Call the original formatter class to do the grunt work
        result = logging.Formatter.format(self, record)

        # Restore the original format configured by the user
        self._style._fmt = format_orig

        return result


def get_logger(logger_name, loglevel, to_stdout=False, create_file=None, writemode="w"):
    formatter = MyFormatter() #logging.Formatter('%(levelname)-8s %(message)s')
    
    log = logging.getLogger(logger_name)
    log.setLevel(loglevel)
    if create_file:
        logf = logging.FileHandler(create_file, writemode)
        logf.setLevel(loglevel)
        logf.setFormatter(formatter)
        log.addHandler(logf)

    if to_stdout:
        # add the handler to the root logger
        console = logging.StreamHandler(sys.stdout)
        console.setLevel(loglevel)
        console.setFormatter(formatter)
        log.addHandler(console)
    return log
