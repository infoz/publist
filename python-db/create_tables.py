#! /usr/bin/env python
import sys
import json
from simplemysql import SimpleMysql
import argparse
from csv2db import csv2db

parser = argparse.ArgumentParser("Create the publist database tables according to sql statements in an sql file")
parser.add_argument("-reset", "--reset", type=bool, default=False, const=True, nargs="?", help="delete all tables before recreating them")
parser.add_argument("-sql", "--sql", type=str, required=True, help="file containing the 'CREATE TABLE' SQL statements")
parser.add_argument("-pis", "--pis", type=str, help="csv file containing data to be loaded into the pi table")
parser.add_argument("-c", "--config", type=str, default="dbconfig_rw.json", help="json file containing host, db, user (with rights to create tables) and pw for the database")
args = parser.parse_args()


with open(args.config) as dbcf:
  dbconfig=json.load(dbcf)

db = SimpleMysql(
  host=dbconfig["host"], 
  db=dbconfig["db"], 
  user=dbconfig["user"], 
  passwd=dbconfig["pw"],
  keep_alive=True
)

tables=[]
with open(args.sql) as f:
  table_sql=""
  for line in f:
    table_sql+=line
    if line.find("CREATE TABLE") != -1:
      tablename=line.split("`")[1]
      tables.append(tablename)

if args.reset:
  for tablename in list(tables)[::-1]:
    sql="DROP TABLE "+tablename+";"
    print("dropping "+tablename)
    db.query(sql)


db.query(table_sql)

if args.pis:
  csv2db("pi", args.pis, dbconfig)