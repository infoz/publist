CREATE TABLE IF NOT EXISTS `pi` (
  `auid` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `name` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `alt_auid1` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `alt_auid2` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `alt_auid3` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `alt_auid4` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `alt_auid5` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `orcid` varchar(19) COLLATE utf8_unicode_ci DEFAULT NULL,
  `institute` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `emeritus` tinyint(1) DEFAULT '0',
  `noprof` tinyint(1) DEFAULT '0',
  `exclude` tinyint(1) DEFAULT '0',
  `exit_year` year(4) DEFAULT 0,
  PRIMARY KEY (`auid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `publications` (
  `eid` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `doi` varchar(80) COLLATE utf8_unicode_ci,
  `authors` mediumtext COLLATE utf8_unicode_ci,
  `journal` mediumtext COLLATE utf8_unicode_ci,
  `volume` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `issue` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pagerange` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `coverdate` date NOT NULL,
  `title` text COLLATE utf8_unicode_ci NOT NULL,
  `pubmed_id` varchar(25) COLLATE utf8_unicode_ci,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(1) DEFAULT '0',
  `date_added` date,
  `orphan` tinyint(1) DEFAULT '0',
  `subtype` varchar(30) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Article',
  PRIMARY KEY (`eid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `pi_to_pubs` (
  `eid` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `auid` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `alt_auid` varchar(15) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `multiple_affiliations` tinyint(1) DEFAULT '0',
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`eid`, `auid`),
  FOREIGN KEY ( `eid` ) REFERENCES `publications` ( `eid` ) ON UPDATE  CASCADE  ON DELETE  CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `affiliations` (
  `afid` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `affilname` text COLLATE utf8_unicode_ci ,
  `country` tinytext COLLATE utf8_unicode_ci ,
  `city` tinytext COLLATE utf8_unicode_ci ,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `main_afid` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`afid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `pi_to_affil` (
  `auid` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `afid` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`auid`, `afid`),
  FOREIGN KEY ( `auid` ) REFERENCES `pi` ( `auid` ) ON UPDATE  CASCADE  ON DELETE  CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `pubs_to_affil` (
  `eid` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `afid` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `auid` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`eid`, `afid`),
  FOREIGN KEY ( `eid` ) REFERENCES `publications` ( `eid` ) ON UPDATE  CASCADE  ON DELETE  CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

CREATE TABLE IF NOT EXISTS `metrics` (
  `eid` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `doi` varchar(80) COLLATE utf8_unicode_ci,
  `citations` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `altmetric_donut` varchar(8) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `altmetric_score` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `altmetric_id` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY ( `eid` ),
  FOREIGN KEY ( `eid` ) REFERENCES `publications` ( `eid` ) ON UPDATE  CASCADE  ON DELETE  CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

