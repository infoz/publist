#! /usr/bin/env python
import json, sys
from simplemysql import SimpleMysql
import argparse

def make_eid(journal, volume, issue, pagerange, authors, title):
  eid=""
  if len(journal.split())>1:
    for w in journal.split():
      eid+=w[0]+"-"
  else:
    eid+=journal[:5]+"-"
  if volume:
    eid+="%s."%volume
  if issue:
    eid+="%s."%issue
  if pagerange:
    eid+="%s."%pagerange.split("-")[0]
  i=0
  for author in authors.split(","):
    eid+=author[0]
    i+=1
    if i>5:
      break
  eid+="."+title[0]
  return eid


def add_publication_to_publist(dbconfig, *, profname, title, authors, journal, coverdate, **kwargs): 
  '''add a publication to the publications table, connect it to the pi in the pi_to_pubs table
    keyword arguments can be any of the fields in the pulication table: 
      title, authors, journal, coverdate, volume, issue, pagerange, doi, pubmed_id, subtype
  '''

  db = SimpleMysql(
    host=dbconfig["host"], 
    db=dbconfig["db"], 
    user=dbconfig["user"], 
    passwd=dbconfig["pw"],
    keep_alive=True
  )
  
  if "subtype" in kwargs and kwargs["subtype"]==None:
    kwargs["subtype"]="Article"

  fields={"authors":authors, "title":title, "journal":journal, "coverdate":coverdate}
  fields.update(kwargs)
  fields["status"]=2 # manually added

  if not fields["eid"]:
    fields["eid"]=make_eid(journal, fields["volume"], fields["issue"], fields["pagerange"], authors, title)
  
  eid_exists=db.getOne(table="publications", fields=["title"], where=("eid = %s", [fields["eid"]]))
  if eid_exists:
    sys.exit("ERROR: an entry with eid %s already exists - if this is not the same publication, choose a new eid" % (fields["eid"]))
  
  pi=db.getOne("pi", ["auid"], ["name = '%s'"%(profname)])
  if not pi:
    sys.exit("ERROR: no entry for  %s in the pi table."%(profname))  
  
  db.insert("publications", fields)
  entry = {"eid": fields["eid"], "auid": pi["auid"]}
  db.insert("pi_to_pubs", entry)
  db.commit()




if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument("-pi", type=str, required=True, help='professor name that exists in the pi table')
  parser.add_argument("-title", type=str, required=True, help='title')
  parser.add_argument("-authors", type=str, required=True, help='')
  parser.add_argument("-journal", type=str, required=True, help='')
  parser.add_argument("-coverdate", type=str, required=True, help='')
  parser.add_argument("-volume", type=str)
  parser.add_argument("-issue", type=str)
  parser.add_argument("-pagerange", type=str)
  parser.add_argument("-doi", type=str)
  parser.add_argument("-pubmed_id", type=str)
  parser.add_argument("-subtype", type=str)
  parser.add_argument("-eid", type=str, help="automatically set if not given (recommended)")
  parser.add_argument("-c", "--config", type=str, default="dbconfig.json", help="json file containing host, db, user and pw for the database")
  args = parser.parse_args()

  with open(args.config) as dbcf:
    dbconfig=json.load(dbcf)

  add_publication_to_publist(dbconfig, profname=args.pi, title=args.title, authors=args.authors, 
                            journal=args.journal, coverdate=args.coverdate, volume=args.volume, 
                            issue=args.issue, pagerange=args.pagerange, doi=args.doi, 
                            pubmed_id=args.pubmed_id, subtype=args.subtype, eid=args.eid)