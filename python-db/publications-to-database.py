#! /usr/bin/env python
# -*- coding: utf-8 -*-
import json
import logging
import os
import sys
import argparse
import html
from datetime import datetime
from tools import get_logger, send_email_with_text, get_profs_from_DB, get_author_publications_from_scopus, get_author_affiliations, clean_title, clean_pagerange
from simplemysql import SimpleMysql
import mysql.connector as mysql
from pybliometrics.scopus import AffiliationRetrieval
from pybliometrics.scopus.exception import Scopus404Error

#-- MODIFY fpath to the path where config.json and dbconfig.json are stored
fpath=os.path.dirname(os.path.realpath(__file__))

# --------------------
with open(fpath+"/dbconfig.json") as f:
    dbconfig = json.load(f)
with open(fpath+"/config.json") as f:
    config = json.load(f)

logfilepath = config["logfilepath"]
logfilename = 'publist.log'

now = datetime.now()
today = now.strftime("%Y-%m-%d")

parser = argparse.ArgumentParser()
parser.add_argument("-start", "--startyear", type=int, default=2009)
parser.add_argument("-end", "--endyear", type=int, default=now.year)
parser.add_argument("-y", "--year", type=int,
                    help="set start and end to this year")
parser.add_argument("-prof", "--profnames", type=str, nargs="*", default=[],
                    help="get data only for professors whose name contains one of these terms")
parser.add_argument("-r", "--refresh", type=int, default=0,
                    help="refresh cached Scopus results list if older than this")
parser.add_argument("-npa", "--newpaperalert", type=bool, default=False,
                    const=True, nargs="?", help="send email alert for new papers")
parser.add_argument("-m", "--sendmail", type=bool, default=False, const=True,
                    nargs="?", help="send logfile per mail and delete it on the server")
parser.add_argument("-debug", type=bool, default=False, const=True, nargs="?")
parser.add_argument("-dryrun", type=bool, default=False, const=True,
                    nargs="?", help="do not commit anything to the database")
parser.add_argument("-s", "--silent", type=bool, default=False, const=True,
                    nargs="?", help="no printing to stdout")
parser.add_argument("-q", "--quiet", type=bool, default=False, const=True,
                    nargs="?", help="reduced verbosity")
args = parser.parse_args()

if args.year:
  args.startyear = args.year
  args.endyear = args.year
startyear = args.startyear
endyear = args.endyear

if args.debug:
    loglevel = logging.DEBUG
else:
    loglevel = logging.INFO

logfile=logfilepath+logfilename
missing_logfolder=False
if logfilepath.strip()=="" :
    logfile=None
elif not os.path.exists(logfilepath):
    logfile=None
    missing_logfolder=True
log = get_logger("", loglevel, to_stdout=not args.silent, create_file=logfile)

if missing_logfolder:
    logging.warning("Logfile path does not exist: %s" % logfilepath)

try:
  dbs = SimpleMysql(
    host=dbconfig["host"],
    db=dbconfig["db"],
    user=dbconfig["user"],
    passwd=dbconfig["pw"],
    keep_alive=True
  )
except mysql.Error as err:
  sys.exit(err)

#
logging.info("#-- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- --")
#logging.info(bcolors.WARNING + "Fetching publications from" + bcolors.ENDC +
#             bcolors.OKGREEN + str(startyear) + bcolors.ENDC +
#             " to "+ bcolors.OKGREEN + str(endyear)+ bcolors.ENDC)
logging.info("# Fetching publications from " + str(startyear)  +
             " to "+ str(endyear))
logging.info("#-- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- --")

if not args.quiet:
    logging.info("# %-25s %-11s %4s %6s %8s" %
        ("professor", "years", "db", "scopus", "orphans"))

num_results_scopus_total = 0
affiliations = {}
profresults = []
messages = []
profsdb = get_profs_from_DB(dbs, args.profnames)
for p in profsdb:
    profname = p['name']
    scopusids = p['auids']
    exit_year = p['exit_year']
    if exit_year == 0:
        exit_year = endyear
    elif exit_year < endyear:
        logging.debug("%s left in %s, only collecting publications until %s" % (
            profname, exit_year, exit_year+2))
        exit_year += 2  # 2 years after e.g. an associate prof left, we stop collecting his/her publications

    author_pubs = {}
    for year in range(startyear, exit_year+1):
        pubs_this_year=get_author_publications_from_scopus(scopusids, year=year, refresh=args.refresh)
        if len(pubs_this_year)>0:
            author_pubs.update(pubs_this_year)
    num_results_scopus = len(author_pubs)
    logging.debug("found %s for %s"% (num_results_scopus, p['name']))

    # get all publications for this prof and timerange from the db, to see if any have become orphans
    sql = "SELECT a.eid FROM publications a, pi_to_pubs b WHERE a.eid = b.eid AND (" + " OR ".join(
        ["b.auid = %s" for x in scopusids]) + ") AND YEAR(a.coverdate) >= %s AND YEAR(a.coverdate) <= %s AND orphan = 0"
    cur = dbs.query(sql, scopusids+[startyear, exit_year])
    result = cur.fetchall()
    eids_in_db = [r[0] for r in result]

    eids_in_scopus = []
    author_affiliations = {}
    for eid, pub in author_pubs.items():
        eids_in_scopus.append(pub.eid)
        if pub.author_names != None:
            authors = pub.author_names.split(";")
            authors = ", ".join([n.split(",")[0]+" "+"".join([x[0]+"." for x in n.split(",")[1].split()])
                            for n in pub.author_names.split(";")])
        else:
            authors=None

        pubdata = {
            "eid": pub.eid,
            "date_added": today,
            "authors": authors,
            "title": clean_title(pub),
            "journal": pub.publicationName,
            "volume": pub.volume,
            "issue": pub.issueIdentifier,
            "pagerange": clean_pagerange(pub),
            "coverdate": pub.coverDate,
            "doi": pub.doi,
            "pubmed_id": pub.pubmed_id,
            "orphan": 0,
            "subtype": pub.subtypeDescription if pub.subtypeDescription else "Article"}

        logging.debug("insertOrUpdate %s"%(pub.eid))
        dbs.insertOrUpdateConditional("publications", pubdata, exclude=["eid", "date_added"], condition="status = 0", conditionalfields=["authors", "journal", "volume", "title", "issue", "pagerange"])

        metrics = {
            "eid": pub.eid,
            "doi": pub.doi,
            "citations": pub.citedby_count
        }
        dbs.insertOrUpdate("metrics", metrics, ["eid"])

        # find prof's author sequence number to be able to assign his affiliation
        if pub.author_ids != None:
            auids = pub.author_ids.split(";")
        else:
            auids=[]
        seq = 0
        for a in auids:
            if a in scopusids:
                alt_auid = a
                break
            seq += 1

        author_aff_pub = get_author_affiliations(pub, seq)
        author_affiliations.update(author_aff_pub)

        pi_to_pubs = {
            "eid": pub.eid,
            "auid": scopusids[0],
            "alt_auid":  alt_auid,
            "multiple_affiliations": 1 if len(author_aff_pub) > 1 else 0
        }

        dbs.insertOrUpdate("pi_to_pubs", pi_to_pubs, ["eid", "auid"])
        if pub.afid: # some papers don't have an affiliation associated

            # sometimes the html-escaping went wrong, we need to unescape twice
            affilname=html.unescape(html.unescape(pub.affilname)).split(";")
            #print(affilname)
            if len(affilname) != len(pub.afid.split(";")):
                logging.debug("The array of afids is not the same length as the array of affiliations names,")
                logging.debug("probably there is an ';' in one of the affiliations' names:\n %s" %(pub.affilname))

                # see if the afid is in the db already
                i=0
                for afid in pub.afid.split(";"):
                    if afid not in affiliations:
                        try:
                            affil_entry=dbs.getOne("affiliations", fields=["afid", "main_afid", "city", "country"], where=("afid=%s", [afid]))
                        except Scopus404Error:
                            logging.warning("Invalid Scopus affiliation id: %s"%(afid))
                        if not affil_entry:
                            # not found, get from scopus
                            aff = AffiliationRetrieval(afid)
                            affiliations[afid] = {"afid": afid, 
                            "city": aff.city,
                            "country": aff.country, 
                            "affilname": aff.affiliation_name,
                            "main_afid": afid}
                            logging.debug("Fetched affiliation from scopus Aff.Retrieval API: %s" %(affiliations[afid]))
                    i+=1

            else:    
                i=0
                for afid in pub.afid.split(";"):
                    if afid not in affiliations:
                        affiliations[afid] = {"afid": afid, 
                        "city": pub.affiliation_city.split(";")[i],
                        "country": pub.affiliation_country.split(";")[i], 
                        "affilname": affilname[i],
                        "main_afid": afid}
                    i+=1
            i=0
            for afid in pub.afid.split(";"):
                pubs_to_affil = {
                    "eid": pub.eid,
                    "auid": scopusids[0],
                    "afid": afid
                }
                dbs.insertOrUpdate("pubs_to_affil", pubs_to_affil, ["eid"])
                i+=1

            # remove entries from pubs_to_affil if the affiliation is no longer associated with the publication
            # this will happen if Scopus cleaned up the affiliation data
            pubaffils_in_db = dbs.getAll("pubs_to_affil", ["eid", "afid"], ("eid = %s", [pub.eid]))
            for r in pubaffils_in_db:
                if r["afid"] not in pub.afid.split(";"):
                    messages.append(f"afid {r['afid']} is not associated to eid {r['eid']} any more, deleting from pubs_to_affil")
                    dbs.delete("pubs_to_affil", where=("eid=%s and afid=%s", [r["eid"], r["afid"]]))

    # find orphans (publications that are in our db and not in the scopus db)
    for eid in eids_in_db:
      if eid not in eids_in_scopus:
        dbs.update("publications", {"orphan": 1}, ["eid='"+eid+"'"])

    for afid in author_affiliations:
      pi_to_affil = {
          "auid": scopusids[0],
          "afid":  afid
      }
      dbs.insertOrUpdate("pi_to_affil", pi_to_affil, ["auid"])

    if not args.dryrun:
      dbs.commit()

    for aafid in author_affiliations.keys():
        if aafid not in affiliations.keys():
            logging.warning("missing aafid in affiliations: %s"%(aafid))
    #affiliations.update(author_affiliations) # should not be necessary, affiliations already contains all affil. for the publication

    num_results_scopus_total += num_results_scopus

    # get number of database entries for this prof and timeframe
    sql = "SELECT COUNT(a.eid) FROM publications a, pi_to_pubs b WHERE a.eid = b.eid AND (" + " OR ".join(
        ["b.auid = %s" for x in scopusids]) + ") AND YEAR(a.coverdate) >= %s AND YEAR(a.coverdate) <= %s"
    cur = dbs.query(sql, scopusids+[startyear, exit_year])
    num_results_db = cur.fetchone()[0]

    # get number of orphans for this prof and timeframe
    sql = "SELECT COUNT(a.doi) FROM publications a, pi_to_pubs b WHERE a.eid = b.eid AND (" + " OR ".join(
        ["b.auid = %s" for x in scopusids]) + ") AND YEAR(a.coverdate) >= %s AND YEAR(a.coverdate) <= %s AND orphan!=0"
    cur = dbs.query(sql, scopusids+[startyear, exit_year])
    orphans = cur.fetchone()[0]

    if not args.quiet:
        profresult = (profname, startyear, exit_year,
                    num_results_db, num_results_scopus, orphans)
        logging.info(" %-25s (%4d-%4d)  %4d %6d %8d " % profresult)


#logging.info(bcolors.WARNING + "Found a total of %s entries in scopus." %
#      (num_results_scopus_total) + bcolors.ENDC)
logging.info("#\n# Found a total of %s entries in scopus." %
      (num_results_scopus_total))

#****** find out if there were papers added today
result = dbs.getAll("publications", ["eid", "coverdate", "authors", "doi", "title",
                                     "journal", "volume", "issue", "pagerange"], ["date_added = curdate()"])
num_newpapers = len(result) if result else 0
mailmsg = "Found %s new paper(s) today:\n\n" % (num_newpapers)
if num_newpapers > 0:    
    for r in result:
        # if the year of the coverdate is not the current year, change date_added to the coverdate
        if (now.year-r["coverdate"].year) > 1:
            update_sql = 'UPDATE publications SET date_added = coverdate WHERE eid = %s;'
            logging.info("  .. setting date_added to coverdate: %s" %
                         (r["coverdate"]))
            dbs.query(update_sql, ([r["eid"]]))
            if not args.dryrun:
              dbs.commit()
        mailmsg += "--\n%s\n" % (r["title"])
        mailmsg += "    %s\n" % (r["authors"])
        mailmsg += "    %s, %s, %s(%s)%s, %s\n\n" % (
            r["journal"], r["doi"], r["volume"], r["issue"], r["pagerange"], r["coverdate"])

    if "website" in config and config["website"].strip() != "":
        mailmsg += "See how it looks in our publication list: %s" %(config["website"])
    sender = config["emailsender"]
    recipients = config["recipients_newpapers"]
    if args.newpaperalert:
        if sender.strip() == "" or sender.find("@") == -1:
            logging.error("invalid email address for emailsender: %s, check your config.json"%(sender))
        for recipient in recipients:
            send_email_with_text(mailmsg, 
                "%s new publication(s) in scopus" % (num_newpapers), recipient, sender)
    logging.info("# -- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----")
    logging.info("\n"+mailmsg)


#****** update the affiliatons table
logging.info("# -- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ---- ----")
#logging.info(bcolors.WARNING + "Found %s affiliations for the professor(s) in these papers." %
#      (len(affiliations)) + bcolors.ENDC)
logging.info("# Found %s affiliations for the professor(s) in these papers." %
      (len(affiliations)))
for afid, a in affiliations.items():
    logging.debug("adding to affiliations: %s, %s, %s, %s" %
                    (afid, a['affilname'], a['country'], a['city']))
    numrows=dbs.insertOrUpdateConditional("affiliations", a, exclude=["afid"], condition="status = 0", conditionalfields=["affilname", "city", "country", "main_afid"])
if not args.dryrun:
  dbs.commit()
dbs.end()
logging.info(
    '# Total new or updated affiliations written to affiliations: %s' % (numrows))

for line in messages:
    logging.info("# "+line)

#send the logfile via email
if args.sendmail:
    sender = config["emailsender"]
    recipient = config["emailrecipient"]
    if recipient.strip() == "" or recipient.find("@") == -1:
        logging.error("invalid email address for emailrecipient: %s, check your config.json"%(recipient))
    if sender.strip() == "" or sender.find("@") == -1:
        logging.error("invalid email address for emailsender: %s, check your config.json"%(recipient))
    else:
        #logging.info(bcolors.OKBLUE + "sending the log to " + bcolors.ENDC + recipient + bcolors.OKBLUE +
        #      " and deleting the logfile " + bcolors.ENDC + bcolors.WARNING+logfilepath+logfilename + bcolors.ENDC)
        logging.info("# sending the log to " + recipient + 
            " and deleting the logfile " + logfilepath+logfilename)

        with open(logfilepath+logfilename, "r") as f:
            ar=f.readlines()
        mailmsg = "".join(ar)
        send_email_with_text(
                mailmsg, "LOG " + __file__, recipient, sender)
        # delete logfile
        os.remove(logfilepath+logfilename)
else:
    #logging.info(bcolors.OKBLUE + "wrote log to " + bcolors.ENDC +
    #      bcolors.WARNING+logfilepath+logfilename + bcolors.ENDC)
    logging.info("# wrote log to " + logfilepath+logfilename)
logging.info("# finished " +datetime.now().strftime("%Y-%m-%d %H:%M"))
