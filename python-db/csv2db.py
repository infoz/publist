#! /usr/bin/env python
import json, csv
from simplemysql import SimpleMysql
import argparse


def csv2db(table,  csvfile, dbconfig, exclude=[], verbose=False, delimiter=",",
           nullify=["NO_AUTHORS", "NO_TITLE", "NO_PUBMEDID", "NO_ISSN", "NO_DOI", "NO_EID", "NO_VOLUME", "NO_ISSUE", "NO_PAGERANGE", "NO_ARTICLENUMBER", "NO_COVERDATE", "NO_JOURNAL"]):
  '''Fill the information from the csvfile into a table of the database, converting certain values to NULLs.
  '''
  with open(csvfile) as f:
    csvreader=csv.reader(f, delimiter=delimiter)
    entries=[]
    fields=next(csvreader)
    
    for row in csvreader:
        for term in nullify:
          for field in row:
            if field == term:
              field="NULL"
        z = zip(fields, row)
        entries.append(dict(z))
  #print(entries)

  db = SimpleMysql(
    host=dbconfig["host"], 
    db=dbconfig["db"], 
    user=dbconfig["user"], 
    passwd=dbconfig["pw"],
    keep_alive=True
  )

  for entry in entries:
    if verbose:
      print("inserting %s"%(entry))
    entry = {k: entry[k] for k in entry if k not in exclude}
    db.insertOrUpdate(table, entry, [])
  db.commit()


if __name__ == "__main__":
  parser = argparse.ArgumentParser("Fill a mysql table according to a given csv-formatted file.")
  parser.add_argument("-csv", "--csv", type=str, required=True, help="csv with header line")
  parser.add_argument("-t", "--table", type=str, required=True, help="table name")
  parser.add_argument("-e", "--exclude", type=str, default=[], help="fields to exclude")
  parser.add_argument("-v", "--verbose", type=bool, default=False, const=True, nargs="?", help="verbose")
  parser.add_argument("-d", "--delim", type=str, default=",", help="delimiter in the csv file")
  parser.add_argument("-c", "--config", type=str, default="dbconfig.json", help="json file containing host, db, user (with rights to create tables) and pw for the database")
  args = parser.parse_args()

  with open(args.config) as dbcf:
    dbconfig=json.load(dbcf)

  csv2db(args.table, args.csv, dbconfig, exclude=args.exclude, verbose=args.verbose, delimiter=args.delim)