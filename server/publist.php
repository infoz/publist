<?php
// adapt the following to the path, where your config.php resides
include 'config.php';

header('Access-Control-Allow-Origin: *');

header('Content-Type: text/html; charset=utf-8');
error_reporting(E_ALL);
ini_set('display_errors', 'on');
ini_set('log_errors', 1);
date_default_timezone_set('Europe/Berlin');

$options = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false,
];
$pdo = new PDO("mysql:host=$dbhost;dbname=$dbname;charset=utf8", $dbuser, $dbpass, $options);
// charset=utf8 works only for php>5.3.6, for lower versions:
//$pdo = new PDO('mysql:host=' . $dbhost . ';dbname=' . $dbname, $dbuser, $dbpass, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));


function logger($meldung, $logfilepath)
{
  $log  = date("Y-m-d G:i:s") . " - " . $meldung . PHP_EOL;
  file_put_contents($logfilepath . '/publist_' . date("Y-m-d") . '.log', $log, FILE_APPEND);
}

try {
  if (isset($_GET["getpis"]) && $_GET["getpis"] == "1") {
    // get the pi names
    $sql = "SELECT auid, name, institute FROM pi WHERE exclude = 0 ";
    $sql_var = array();
    if (isset($_GET["rgname"]) && $_GET["rgname"] != "") {
      $sql = $sql . ' AND name = :rgname '; 
      $sql_var[":rgname"]=$_GET["rgname"];
    } elseif (isset($_GET["institute"]) && $_GET["institute"] != "") {
      $sql = $sql . ' AND institute = :institute'; 
      $sql_var[":institute"]=$_GET["institute"];
    }
    $sql = $sql . " ORDER by name";
    // Prepare the paged query
    $stmt = $pdo->prepare($sql);
    $stmt->execute($sql_var);

    // Do we have any results?
    if ($stmt->rowCount() > 0) {
      // Define how we want to fetch the results
      $stmt->setFetchMode(PDO::FETCH_ASSOC);
      $iterator = new IteratorIterator($stmt);
      $piarray = array();

      foreach ($iterator as $row) {
        $piarray[] = $row;
      }
      echo json_encode($piarray);
    } else {
      echo json_encode(array('total' => "0"));
    }
  } else {
    $sql = "SELECT pub.title, pub.doi, pub.authors, pub.journal, pub.volume, pub.issue, pub.pagerange, pub.coverdate, pub.date_added FROM publications pub "; 
    $subquery = "SELECT pp.eid FROM pi_to_pubs pp, pi WHERE pp.auid=pi.auid AND pi.exclude=0 ";
    $sql_var=array();

    $myArray[] = array();

    /* ---- process request query string parameters ---- */
    if (isset($_GET["rgroup"]) && $_GET["rgroup"] != "") {
      $rgroup = utf8_decode($_GET["rgroup"]);
      if ($_GET["rgroup"] != "all") {
        $subquery = $subquery . ' AND pp.auid = :rgroup '; 
        $sql_var[":rgroup"] = $rgroup;
      }
    } else {
      $rgroup = "";
    }

    if (isset($_GET["institute"]) && $_GET["institute"] != "") {
      $institute = preg_replace("/[^A-Za-z]/", '', utf8_decode($_GET["institute"]));
      $subquery = $subquery . " AND pi.institute = :institute ";
      $sql_var[":institute"] = $institute;
    } else {
      $institute = "";
    }
    $where = "WHERE pub.eid in ($subquery) ";

    if (isset($_GET["title"]) && $_GET["title"] != "") {
      $title = utf8_decode($_GET["title"]);
      $title_search = "%$title%";
      $where = $where . " AND pub.title LIKE :title ";
      $sql_var[":title"] = $title_search;
    } else {
      $title = "";
    }

    if (isset($_GET["limit"]) && $_GET["limit"] != "") {
      $limit = (int)$_GET["limit"];
    } else {
      $limit = 50;
    }

    if (isset($_GET["authors"]) && $_GET["authors"] != "") {
      $authors = utf8_decode($_GET["authors"]);
      $authors_search = "%$authors%";
      $where = $where . " AND pub.authors LIKE :authors ";
      $sql_var[":authors"] = $authors_search;
    } else {
      $authors = "";
    }

    if (isset($_GET["year"]) && $_GET["year"] != "") {
      //clean the year entry
      $year = preg_replace("/[^1234567890-]/", "", $_GET["year"]);
      if (strlen($year) == 9 && substr_count($year, "-") == 1 && substr($year, 4, 1) == '-') {
        // this is a time range
        $startyear=substr($year, 0, 4);
        $endyear=substr($year, 5, 4);
        $where = $where . " AND pub.coverdate BETWEEN :startdate AND :enddate ";
        $sql_var[":startdate"] = $startyear . "-01-01";
	$sql_var[":enddate"] = $endyear . "-12-31";
      } elseif (strlen($year) == 4) {
        // this is a single year
        $where = $where . " AND pub.coverdate BETWEEN :startdate AND :enddate ";
        $sql_var[":startdate"] = $year . "-01-01";
        $sql_var[":enddate"] = $year . "-12-31";
      } else {
        //forget about the year, this was an erroneous date!
        $year = "";
      }
    } else {
      $year = "";
    }

    $where = $where . " AND pub.status!=3 AND pub.subtype != 'Erratum'";
    $sql = $sql . $where . " ORDER BY pub.date_added DESC";

    /* ---- determine how many items are in the table ---- */
    $countsql = "SELECT COUNT(pub.eid) as total FROM publications pub " . $where;
    $stmt = $pdo->prepare($countsql);
    $stmt->execute($sql_var);
    $result=$stmt->fetch();

    $total = $result['total'];

    // how many pages will there be
    $pages = ceil($total / $limit);
    // what page are we currently on?
    $page = min($pages, filter_input(INPUT_GET, 'page', FILTER_VALIDATE_INT, array(
      'options' => array(
        'default'   => 1,
        'min_range' => 1,
      ),
    )));

    // calculate the offset for the query
    $offset = ($page - 1)  * $limit;

    // Some information to display to the user
    $start = $offset + 1;
    $end = min(($offset + $limit), $total);

    // the "back" link
    $prevlink = ($page > 1) ? '<a href="?page=1" title="First page">&laquo;</a> <a href="?page=' . ($page - 1) . '" title="Previous page">&lsaquo;</a>' : '<span class="disabled">&laquo;</span> <span class="disabled">&lsaquo;</span>';
    $prev = ($page > 1) ? ($page - 1) : 'none';
    $myArray['prevlink'] = $prev;

    // the "forward" link
    $nextlink = ($page < $pages) ? '<a href="?page=' . ($page + 1) . '" title="Next page">&rsaquo;</a> <a href="?page=' . $pages . '" title="Last page">&raquo;</a>' : '<span class="disabled">&rsaquo;</span> <span class="disabled">&raquo;</span>';
    $next = ($page < $pages) ? ($page + 1) : 'none';

    $myArray['nextlink'] = $next;
    // display the paging information
    $myArray['total'] = $total;
    $myArray['page'] = $page;
    $myArray['pages'] = $pages;
    $myArray['start'] = $start;
    $myArray['end'] = $end;
    $myArray['limit'] = $limit;
    $myArray['title'] = $title;
    $myArray['authors'] = $authors;
    $myArray['rgroup'] = $rgroup;
    $myArray['year'] = $year;
    $myArray['institute'] = $institute;

    if (isset($_GET["togglealt"]) && $_GET["togglealt"] == "1") {
      $myArray['togglealt'] = "1";
      $togglealt = "1";
    } else {
      $myArray['togglealt'] = "0";
      $togglealt = "0";
    }

    $links = 5;

    $firstlink = (($page - $links) > 0) ? $page - $links : 1;
    $lastlink = (($page + $links) < $pages) ? $page + $links + ((($links - $page) >= 1) ? $links - $page : 0) : $pages;

    $html       = '<ul>';

    if ($page == 1) {
      $html       .= '<li class = "activepage">&laquo;</li>';
    } else {
      $html       .= '<li><a href="?limit=' . $limit . '&page=' . ($page - 1) . '&rgroup=' .  $rgroup . '&title=' .  $title . '&authors=' .  $authors . '&year=' .  $year . '&institute=' .  $institute .  '&togglealt=' .  $togglealt . '">&laquo;</a></li>';
    }

    if ($firstlink > 1) {
      $html   .= '<li><a href="?limit=' . $limit . '&page=1' . '&rgroup=' .  $rgroup . '&title=' .  $title . '&authors=' .  $authors . '&year=' .  $year . '&institute=' .  $institute .  '&togglealt=' .  $togglealt . '">1</a></li>';
      $html   .= '<li class = "threedots"><span>...</span></li>';
    }

    for ($i = $firstlink; $i <= $lastlink; $i++) {
      if ($page == $i) {
        $html       .= '<li class = "activepage">' . $i . '</li>';
      } else {
        $html   .=  '<li><a href="?limit=' . $limit . '&page=' . $i . '&rgroup=' .  $rgroup . '&title=' .  $title . '&authors=' .  $authors . '&year=' .  $year . '&institute=' .  $institute .  '&togglealt=' .  $togglealt . '">' . $i . '</a></li>';
      }
    }

    if ($lastlink < $pages) {
      $html   .= '<li class = "threedots"><span>...</span></li>';
      $html   .= '<li><a href="?limit=' . $limit . '&page=' . $pages . '&rgroup=' .  $rgroup . '&title=' .  $title . '&authors=' .  $authors . '&year=' .  $year . '&institute=' .  $institute .  '&togglealt=' .  $togglealt . '">' . $pages . '</a></li>';
    }

    if ($page == $pages) {
      $html   .= '<li class = "activepage">&raquo;</li>';
    } else {
      $html   .= '<li><a href="?limit=' . $limit . '&page=' . ($page + 1) . '&rgroup=' .  $rgroup . '&title=' .  $title . '&authors=' .  $authors . '&year=' .  $year . '&institute=' .  $institute .  '&togglealt=' .  $togglealt . '">&raquo;</a></li>';
    }

    $html       .= '</ul>';

    if ($pages > 1) {
      $myArray['pagination'] = $html;
    }

    // Prepare the paged query
    $stmt = $pdo->prepare($sql . ' LIMIT :limit OFFSET :offset');
    // Bind the query params
    $sql_var[":limit"] = $limit;
    $sql_var[":offset"] = $offset;
    $stmt->execute($sql_var);

    // Do we have any results?
    if ($stmt->rowCount() > 0) {
      // Define how we want to fetch the results
      $stmt->setFetchMode(PDO::FETCH_ASSOC);
      $iterator = new IteratorIterator($stmt);
      $pubarray = array();
      // Display the results
      foreach ($iterator as $row) {
        $pubarray[] = $row;
      }

      $myArray['publications'] = $pubarray;
      echo json_encode($myArray);
    } else {
      echo json_encode(array('total' => "0"));
    }
  }
  logger($_SERVER['QUERY_STRING'], $logfilepath);
} catch (Exception $e) {
  echo '<p>', $e->getMessage(), '</p>';
  logger($e->getMessage(), $logfilepath);
}
